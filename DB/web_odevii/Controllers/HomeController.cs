﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web_odevii.DataBase;
using web_odevii.Models;


namespace web_odevii.Controllers
{
    public class HomeController : Controller
    {
        HalisahaContext db = new HalisahaContext();
       
        // GET: Home
        public ActionResult Anasayfa()
        {
            var veriler = db.Halisahalar.OrderByDescending(x => x.HalisahaRank).ToList();
            
            return View(veriler);
        }
        public ActionResult RandevuAl(int id)
        {
            if (Session["kullanici"] == null)
            {
                return RedirectToAction("Anasayfa");
            }
            else
            {
                var sorgu = db.Halisahalar.FirstOrDefault(x => x.HalisahaID == id);
             //   var liste = db.Randevular.Where(x => x.HalisahaId == id).ToList();
              //  ViewBag.randevu = liste;
               
                return View(sorgu);
            }
            
        }

        //[HttpPost]
        //public ActionResult RandevuAl(Randevu randevu,string RandevuSaati,int HalisahaID)
        //{
            
        //    DateTime date = randevu.RandevuTarih;
        //    double saat = Convert.ToDouble(RandevuSaati);
        //    date= date.AddHours(saat);

        //    var hid = ViewBag.id;


        //    Randevu r = new Randevu();

        //    r.RandevuTarih = date;
        //  //  r.KullaniciId = Convert.ToInt32(Session["kid"]);
        //    //r.HalisahaId = HalisahaID;
             

        //    db.Randevular.Add(r);

        //    db.SaveChanges();

        //    return RedirectToAction("Anasayfa");
        //}

        [HttpGet]
        public ActionResult GirisYap()
        {
            if (Session["kullanici"] == null)
            {
                return View();
            }
            else
                return RedirectToAction("Anasayfa");
        }
        [HttpPost]

        public ActionResult GirisYap(string KEmail, string KParola)
        {
            Kullanici k = new Kullanici();

            var sorgu = db.Kullanicilar.Where(x => x.KullaniciEmail == KEmail).FirstOrDefault();

            if(sorgu != null)
            {
                if(sorgu.KullaniciParola == KParola && sorgu.KullaniciEmail == KEmail)
                {
                    Session["kullanici"] = sorgu.KullaniciAdi;
                    Session["kid"] = sorgu.KullaniciID;
                }
                else
                {
                    ViewBag.sifre = "Girdiğiniz Parola Hatalıdır!";
                    return View();
                }
            }
            else
            {
              
                ViewBag.q = "Kayıtlı kullanıcı bulunamadı!";
                return View();
            }
           

            return RedirectToAction("Anasayfa");

        }

        public ActionResult CikisYap()
        {
            Session["kullanici"] = null;
            Session["kid"] = null;

            return RedirectToAction("Anasayfa");
        }




        #region UyeOl()

        public ActionResult UyeOl()
        {
            return View();
        }

        #endregion

        [HttpPost]
        #region UyeOl()

        public ActionResult UyeOl(Kullanici kullanici)
        {
            var emailkontrol = db.Kullanicilar.Where(x => x.KullaniciEmail == kullanici.KullaniciEmail).FirstOrDefault();
            
            if(kullanici.KullaniciAdi == ""|| kullanici.KullaniciEmail == "" || kullanici.KullaniciParola == "" || kullanici.KullaniciSoyadi == "" || kullanici.KullaniciTelefonNo == "")
            {
                ViewBag.bosluk = "Lütfen Formda Boş Yer Bırakmayınız.";
                return View();
            }        
            else
            {
                if (emailkontrol != null)
                {
                    ViewBag.email = "Girdiğiniz E-mail Kullanılmaktadır.";
                    return View();
                }
                else
                {
                    Kullanici k = new Kullanici();

                    k.KullaniciAdi = kullanici.KullaniciAdi;
                    k.KullaniciEmail = kullanici.KullaniciEmail;
                    k.KullaniciParola = kullanici.KullaniciParola;
                    k.KullaniciSoyadi = kullanici.KullaniciSoyadi;
                    k.KullaniciTelefonNo = kullanici.KullaniciTelefonNo;
                    k.KullaniciYetki = "kullanici";

                    db.Kullanicilar.Add(k);

                    db.SaveChanges();

                    Session["kullanici"] = kullanici.KullaniciAdi;

                    return RedirectToAction("Anasayfa");

                }
            }
           


            
        }

        #endregion








        public ActionResult Iletisim()
        {
            return View();
        }
    }
}