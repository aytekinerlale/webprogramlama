﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace web_odevii.Models
{
    public class Kullanici
    {
        public int KullaniciID { get; set; }
        
        public string KullaniciAdi { get; set; }
        public string KullaniciSoyadi { get; set; }
        public string KullaniciTelefonNo { get; set; }
        public string KullaniciYetki { get; set; }

        public string KullaniciEmail { get; set; }
        public string KullaniciParola { get; set; }

        public virtual ICollection<Halisaha> Halisahalar { get; set; }
        public virtual ICollection<Randevu> Randevular { get; set; }
    }
}