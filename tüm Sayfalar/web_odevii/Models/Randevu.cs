﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_odevii.Models
{
    public class Randevu
    {
        public int RandevuID { get; set; }

        public DateTime RandevuTarih { get; set; }

        public Kullanici KullaniciId { get; set; }

        public Halisaha HalisahaId { get; set; }

    }
}