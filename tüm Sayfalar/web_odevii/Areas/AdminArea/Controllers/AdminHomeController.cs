﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web_odevii.DataBase;
using web_odevii.Models;

namespace web_odevii.Areas.AdminArea.Controllers
{
    public class AdminHomeController : Controller
    {
        HalisahaContext db = new HalisahaContext();
        // GET: AdminArea/AdminHome
        public ActionResult Index()
        {
            return View();
        }

        #region Kullanicilar İşlemler
        
        
        #region Kullanicilar()
        public ActionResult Kullanicilar()
        {
            var item = db.Kullanicilar.ToList();
            return View(item);
        }
        #endregion

        [HttpPost]
        #region Kullanicilar(Kullanici kullanici)

        public ActionResult Kullanicilar(Kullanici kullanici)
        {
            return View();
        }

        #endregion

        #region KullaniciEkle()

        public ActionResult KullaniciEkle()
        {
            return View();
        }

        #endregion

        [HttpPost]
        #region KullaniciEkle(Kullanici kullanici) formdan gelen bilgiler veri tabanına kaydediliyor

        public ActionResult KullaniciEkle(Kullanici kullanici)
        {
            Kullanici k = new Kullanici();
            k.KullaniciAdi = kullanici.KullaniciAdi;
            k.KullaniciSoyadi = kullanici.KullaniciSoyadi;
            k.KullaniciEmail = kullanici.KullaniciEmail;
            k.KullaniciParola = kullanici.KullaniciParola;
            k.KullaniciTelefonNo = kullanici.KullaniciTelefonNo;
            k.KullaniciYetki = kullanici.KullaniciYetki;

            db.Kullanicilar.Add(k);
            db.SaveChanges();
            return RedirectToAction("Kullanicilar");

        }

        #endregion


        #region KullaniciDuzenle(int id) gelen id ye gore kullanıcıyı duzenleme sayfası

        public ActionResult KullaniciDuzenle(int id)
        {
            Kullanici kullaniciDuzenle = db.Kullanicilar.FirstOrDefault(x => x.KullaniciID == id);

            return View(kullaniciDuzenle);
        }

        #endregion

        [HttpPost]
        #region KullaniciDuzenle(Kullanici kullanici) formdan gelen bilgiler Duzenlenip veri tabanına kaydediliyor

        public ActionResult KullaniciDuzenle(Kullanici kullanici)
        {

            Kullanici k = db.Kullanicilar.First(x => x.KullaniciID == kullanici.KullaniciID);

            k.KullaniciAdi = kullanici.KullaniciAdi;
            k.KullaniciSoyadi = kullanici.KullaniciSoyadi;
            k.KullaniciEmail = kullanici.KullaniciEmail;
            k.KullaniciParola = kullanici.KullaniciParola;
            k.KullaniciTelefonNo = kullanici.KullaniciTelefonNo;
            k.KullaniciYetki = kullanici.KullaniciYetki;

            db.SaveChanges();

            return RedirectToAction("Kullanicilar");

        }

        #endregion


        #region KullaniciSil(int id)

        public ActionResult KullaniciSil(int id)
        {
            Kullanici kullaniciSilinecek = db.Kullanicilar.FirstOrDefault(x => x.KullaniciID == id);
            
            db.Kullanicilar.Remove(kullaniciSilinecek);

            db.SaveChanges();

            return RedirectToAction("Kullanicilar");
        }

        #endregion

        #endregion


        #region Halisahalar işlemler

        #region Halisahalar()
        // Halisahalar viewi
        public ActionResult Halisahalar()
        {
            var veriler = db.Halisahalar.ToList();
            return View(veriler);
        }

        #endregion




        #region Halisahaekle()

        //burasının asagıdaki action resulttan farkı 
        //burası formun oldugu sayfa duz bir view
        //post methoduyla aynı isimdeki aşağıdaki method
        //ebu methodun viewindeki ekle butonuna tıklandığında bilgileri post 
        //ile o actionresult a gondereecek ve işlemler yapılacaktır.
        //burası ise duz bir sayfa
        public ActionResult HalisahaEkle()
        {
            return View();
        }

        #endregion
        //gelen bilgilerin post ile geleceğini soyluyoruz 
        //buraya bir şey yazmazsak default olarak [HttpGet] olarak get methodu ile bilgi aktarır
        [HttpPost]
        #region HalisahaEkle(Halisaha halisaha) 


        //post ile halisaha bilgileri paket halinde halisaha değişkeninin 
        //içine atanıyor burada Halisaha donus tipi normal bir classtan nesne 
        //alınıyor gibi dusunulmelidir.
        public ActionResult HalisahaEkle(Halisaha halisaha, HttpPostedFileBase HalisahaResimYol)
        {

            Kullanici query = db.Kullanicilar.First(x => x.KullaniciID == halisaha.HalisahaSahibi.KullaniciID);
            //paket halinde nesne parametre olarak geldi ve yeni h adında nesne oluşturduk
            Halisaha h = new Halisaha();

            //oluşturdugumuz yeni nesnenin icine postan gelen bilgileri atıyoruz
            h.HalisahaAdi = halisaha.HalisahaAdi;
            h.HalisahaAdres = halisaha.HalisahaAdres;
            h.HalisahaTelNo = halisaha.HalisahaTelNo;
            h.HalisahaUcret = halisaha.HalisahaUcret;
            h.HalisahaResimYol = HalisahaResimEkle(HalisahaResimYol);
            h.HalisahaSahibi = query;

            //posttan gelen bilgiler paket halinde k degislkeninin icine atandı
            //db.Halisahalar.add diyerek k yı paket halinde yani nesne olarak 
            //veritabanındaki halisahalar tablosuna yeni bir satır olarak ekledik
            db.Halisahalar.Add(h);
            //veri tabanındaki değişiklikleri kaydediyoruz
            db.SaveChanges();
            //yonlendirme olarak sayfayı halisahalar sayfasına yonlendiriyoruz 
            //return redirecttoroute...
            return RedirectToAction("Halisahalar");
        }


        private string HalisahaResimEkle(HttpPostedFileBase exampleInputFile)
        {
            Image image = Image.FromStream(exampleInputFile.InputStream);
            Bitmap bimage = new Bitmap(image, new Size { Width = 900, Height = 400 });
            string uzanti = System.IO.Path.GetExtension(exampleInputFile.FileName);
            string isim = Guid.NewGuid().ToString().Replace("-", "");
            string yol = "/Areas/AdminArea/AdminContent/images" + isim + uzanti;
            bimage.Save(Server.MapPath(yol));
            return yol;
        }
        #endregion

        #region HalisahaDüzenle()

   

        #endregion

        public ActionResult HalisahaDuzenle(int id)
        {
            Halisaha halisahaDuzenle = db.Halisahalar.FirstOrDefault(x => x.HalisahaID == id);

            return View(halisahaDuzenle);
        }

        [HttpPost]
        #region HalisahaDuzenle 

        public ActionResult HalisahaDuzenle(Halisaha halisaha, HttpPostedFileBase HalisahaResimYol)
        {
            Kullanici query = db.Kullanicilar.First(x => x.KullaniciID == halisaha.HalisahaSahibi.KullaniciID);

            Halisaha h = new Halisaha();

            h.HalisahaAdi = halisaha.HalisahaAdi;
            h.HalisahaAdres = halisaha.HalisahaAdres;
            h.HalisahaTelNo = halisaha.HalisahaTelNo;
            h.HalisahaUcret = halisaha.HalisahaUcret;
            h.HalisahaResimYol = HalisahaResimEkle(HalisahaResimYol);

            db.SaveChanges();

            return RedirectToAction("Halisahalar");
        }

        #endregion

        #region HalisahaSil()
          [HttpPost]
        public ActionResult HalisahaSil(int id)
        {
            
            Halisaha halisahaSilinecek = db.Halisahalar.FirstOrDefault(x => x.HalisahaID == id);

            db.Halisahalar.Remove(halisahaSilinecek);

            db.SaveChanges();

            return RedirectToAction("Halisahalar");
        }




        #endregion


#endregion
        // Randevular viewi
        public ActionResult Randevular()
        {
           
            return View();
        }

       

    }

    
}