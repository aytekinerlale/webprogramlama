﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using web_odevii.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace web_odevii.DataBase
{
    public class HalisahaContext:DbContext
    {
        public HalisahaContext():base("HalisahaContext")
        {

        }

        public DbSet<Halisaha> Halisahalar { get; set; }
        public DbSet<Kullanici> Kullanicilar { get; set; }
        public DbSet<Randevu> Randevular { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)//Kayit(s) Falan oluşturmasını engellemek için ezdirme yaptık
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        
    }
}