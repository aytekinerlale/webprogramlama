namespace web_odevii.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class halisaharesimyolueklendi : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Halisaha", "HalisahaResimYol", c => c.String());
            AddColumn("dbo.Halisaha", "HalisahaRank", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Halisaha", "HalisahaRank");
            DropColumn("dbo.Halisaha", "HalisahaResimYol");
        }
    }
}
