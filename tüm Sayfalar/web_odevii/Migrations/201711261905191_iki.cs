namespace web_odevii.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class iki : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Kullanici", "KullaniciEmail", c => c.String());
            AddColumn("dbo.Kullanici", "KullaniciParola", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Kullanici", "KullaniciParola");
            DropColumn("dbo.Kullanici", "KullaniciEmail");
        }
    }
}
