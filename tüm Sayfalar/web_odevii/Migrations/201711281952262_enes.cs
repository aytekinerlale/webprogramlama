namespace web_odevii.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class enes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Halisaha", "HalisahaSahibi_KullaniciID", "dbo.Kullanici");
            DropIndex("dbo.Halisaha", new[] { "HalisahaSahibi_KullaniciID" });
            RenameColumn(table: "dbo.Halisaha", name: "HalisahaSahibi_KullaniciID", newName: "KullaniciID");
            AlterColumn("dbo.Halisaha", "KullaniciID", c => c.Int(nullable: false));
            CreateIndex("dbo.Halisaha", "KullaniciID");
            AddForeignKey("dbo.Halisaha", "KullaniciID", "dbo.Kullanici", "KullaniciID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Halisaha", "KullaniciID", "dbo.Kullanici");
            DropIndex("dbo.Halisaha", new[] { "KullaniciID" });
            AlterColumn("dbo.Halisaha", "KullaniciID", c => c.Int());
            RenameColumn(table: "dbo.Halisaha", name: "KullaniciID", newName: "HalisahaSahibi_KullaniciID");
            CreateIndex("dbo.Halisaha", "HalisahaSahibi_KullaniciID");
            AddForeignKey("dbo.Halisaha", "HalisahaSahibi_KullaniciID", "dbo.Kullanici", "KullaniciID");
        }
    }
}
