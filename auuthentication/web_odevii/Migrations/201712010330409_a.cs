namespace web_odevii.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class a : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Halisaha", "HalisahaTelNo", c => c.String());
            AlterColumn("dbo.Kullanici", "KullaniciTelefonNo", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Kullanici", "KullaniciTelefonNo", c => c.Int(nullable: false));
            AlterColumn("dbo.Halisaha", "HalisahaTelNo", c => c.Int(nullable: false));
        }
    }
}
