namespace web_odevii.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ilk : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Halisaha",
                c => new
                    {
                        HalisahaID = c.Int(nullable: false, identity: true),
                        HalisahaTelNo = c.Int(nullable: false),
                        HalisahaAdi = c.String(),
                        HalisahaUcret = c.Int(nullable: false),
                        HalisahaAdres = c.String(),
                        HalisahaSahibi_KullaniciID = c.Int(),
                    })
                .PrimaryKey(t => t.HalisahaID)
                .ForeignKey("dbo.Kullanici", t => t.HalisahaSahibi_KullaniciID)
                .Index(t => t.HalisahaSahibi_KullaniciID);
            
            CreateTable(
                "dbo.Kullanici",
                c => new
                    {
                        KullaniciID = c.Int(nullable: false, identity: true),
                        KullaniciAdi = c.String(),
                        KullaniciSoyadi = c.String(),
                        KullaniciTelefonNo = c.Int(nullable: false),
                        KullaniciYetki = c.String(),
                    })
                .PrimaryKey(t => t.KullaniciID);
            
            CreateTable(
                "dbo.Randevu",
                c => new
                    {
                        RandevuID = c.Int(nullable: false, identity: true),
                        RandevuTarih = c.DateTime(nullable: false),
                        HalisahaId_HalisahaID = c.Int(),
                        KullaniciId_KullaniciID = c.Int(),
                    })
                .PrimaryKey(t => t.RandevuID)
                .ForeignKey("dbo.Halisaha", t => t.HalisahaId_HalisahaID)
                .ForeignKey("dbo.Kullanici", t => t.KullaniciId_KullaniciID)
                .Index(t => t.HalisahaId_HalisahaID)
                .Index(t => t.KullaniciId_KullaniciID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Randevu", "KullaniciId_KullaniciID", "dbo.Kullanici");
            DropForeignKey("dbo.Randevu", "HalisahaId_HalisahaID", "dbo.Halisaha");
            DropForeignKey("dbo.Halisaha", "HalisahaSahibi_KullaniciID", "dbo.Kullanici");
            DropIndex("dbo.Randevu", new[] { "KullaniciId_KullaniciID" });
            DropIndex("dbo.Randevu", new[] { "HalisahaId_HalisahaID" });
            DropIndex("dbo.Halisaha", new[] { "HalisahaSahibi_KullaniciID" });
            DropTable("dbo.Randevu");
            DropTable("dbo.Kullanici");
            DropTable("dbo.Halisaha");
        }
    }
}
