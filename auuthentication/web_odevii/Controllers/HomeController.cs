﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web_odevii.DataBase;
using web_odevii.Models;


namespace web_odevii.Controllers
{
    public class HomeController : Controller
    {
        HalisahaContext db = new HalisahaContext();

        // GET: Home
        #region Anasayfa
        public ActionResult Anasayfa()
        {
            //Ucretine göre sıralı sekilde viewe listeyi gönderdik
            var veriler = db.Halisahalar.OrderByDescending(x => x.HalisahaUcret).ToList();

            return View(veriler);
        }
        #endregion
 

        [HttpGet]
        #region GirisYap
        public ActionResult GirisYap()
        {
            if (Session["kullanici"] == null)
            {
                return View();
            }
            else
                return RedirectToAction("Anasayfa");
        }
        #endregion

        [HttpPost]
        #region GirisYap
        public ActionResult GirisYap(string KEmail, string KParola)
        {
            Kullanici k = new Kullanici();

            var sorgu = db.Kullanicilar.Where(x => x.KullaniciEmail == KEmail).FirstOrDefault();

            if (sorgu != null)
            {
                if (sorgu.KullaniciParola == KParola && sorgu.KullaniciEmail == KEmail)
                {
                    Session["kullanici"] = sorgu.KullaniciAdi;
                    Session["kid"] = sorgu.KullaniciID;
                    Session["yetki"] = sorgu.KullaniciYetki;
                }
                else
                {
                    ViewBag.sifre = "Girdiğiniz Parola Hatalıdır!";
                    return View();
                }
            }
            else
            {
                ViewBag.q = "Kayıtlı kullanıcı bulunamadı!";
                return View();
            }

            return RedirectToAction("Anasayfa");

        }

        #endregion

        #region CikisYap
        public ActionResult CikisYap()
        {
            Session.Abandon();

            Session["kullanici"] = null;
            Session["kid"] = null;
            Session["yetki"] = null;

            return RedirectToAction("Anasayfa");
        }

        #endregion

        #region randevual
        public ActionResult RandevuAl(int id)
        {
            if (Session["kullanici"] == null)
            {
                return RedirectToAction("Anasayfa");
            }
            else
            {
                var sorgu = db.Halisahalar.FirstOrDefault(x => x.HalisahaID == id);

                return View(sorgu);
            }

        }

        #endregion

        [HttpPost]
        #region randevual
        public ActionResult RandevuAl(Randevu randevu, string RandevuSaati, int HalisahaID)
        {

            DateTime date = randevu.RandevuTarih;
            double saat = Convert.ToDouble(RandevuSaati);
            date = date.AddHours(saat);

            var hid = ViewBag.id;


            Randevu r = new Randevu();

            r.RandevuTarih = date;
       //   r.KullaniciId.KullaniciID = Convert.ToInt32(Session["kid"].ToString());
            r.HalisahaId.HalisahaID = HalisahaID;

            db.Randevular.Add(r);

            db.SaveChanges();

            return RedirectToAction("Anasayfa");



          

        }

        #endregion


        #region UyeOl()

        public ActionResult UyeOl()
        {
            return View();
        }

        #endregion

        [HttpPost]
        #region UyeOl()

        public ActionResult UyeOl(Kullanici kullanici)
        {
            var emailkontrol = db.Kullanicilar.Where(x => x.KullaniciEmail == kullanici.KullaniciEmail).FirstOrDefault();

            if (kullanici.KullaniciAdi == "" || kullanici.KullaniciEmail == "" || kullanici.KullaniciParola == "" || kullanici.KullaniciSoyadi == "" || kullanici.KullaniciTelefonNo == "")
            {
                ViewBag.bosluk = "Lütfen Formda Boş Yer Bırakmayınız.";
                return View();
            }
            else
            {
                if (emailkontrol != null)
                {
                    ViewBag.email = "Girdiğiniz E-mail Kullanılmaktadır.";
                    return View();
                }
                else
                {
                    Kullanici k = new Kullanici();

                    k.KullaniciAdi = kullanici.KullaniciAdi;
                    k.KullaniciEmail = kullanici.KullaniciEmail;
                    k.KullaniciParola = kullanici.KullaniciParola;
                    k.KullaniciSoyadi = kullanici.KullaniciSoyadi;
                    k.KullaniciTelefonNo = kullanici.KullaniciTelefonNo;
                    k.KullaniciYetki = "kullanici";

                    db.Kullanicilar.Add(k);

                    db.SaveChanges();

                    Session["kullanici"] = kullanici.KullaniciAdi;

                    return RedirectToAction("Anasayfa");

                }
            }

        }

        #endregion

        #region Iletisim
        public ActionResult Iletisim()
        {
            return View();
        }

        #endregion
    }
}