﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_odevii.Models
{
    public class Halisaha
    {
        public int HalisahaID { get; set; }
        public string HalisahaTelNo { get; set; }
        public string HalisahaAdi{ get; set; }
        public int HalisahaUcret { get; set; }
        public string HalisahaAdres { get; set;}
        public string HalisahaResimYol { get; set; }
        public int HalisahaRank { get; set; }

        public int KullaniciID { get; set; }
        public virtual Kullanici HalisahaSahibi { get; set; }
    }
}